<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Consultas';
?>
<div class="site-index">
    <div class="jumbotron">
        <hl>Consultas</h1>
    </div>
    <div class="body-content">
        <div class="row">

            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p>Listar los nombres de los dioses</p>
                        <p>                   
                            <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 2</h3>
                        <p>Indica el nombre de los dioses que el numero de caracteres del nombre sea mayor que 8</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
                <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 3</h3>
                        <p>Indica el numero total de dioses</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
         <div class="row">
               <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 4</h3>
                        <p>Indica el numero total de modos</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
                <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 5</h3>
                        <p>Seleccionar el numero de dioses de cada categoria</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 6</h3>
                        <p>Seleccionar el numero de muertes de cada modo</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
         </div>
        <div class="row">
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 7</h3>
                        <p>Seleccionar el numero de asistencias que se producen en cada modo de partida</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 8</h3>
                        <p>Seleccionar el nombre de los dioses de la partida 1</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
                 <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 9</h3>
                        <p>Indicar que dios no ha participado en ninguna partida</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
               <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 10</h3>
                        <p>Indica el dios que haya hecho mas bajas en una partida</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default']) ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
