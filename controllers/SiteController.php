<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nombre FROM dios',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nombre'],
                    "titulo" => "Consulta 1",
                    "enunciado" => "Listar los nombres de los dioses",
                    "sql" => 'SELECT nombre FROM dios',
        ]);
    }
    
    public function actionConsulta2() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nombre FROM dios HAVING CHAR_LENGTH(nombre)>8',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nombre'],
                    "titulo" => "Consulta 2",
                    "enunciado" => "Indica el nombre de los dioses que el numero de caracteres del nombre sea mayor que 8",
                    "sql" => 'SELECT nombre FROM dios HAVING CHAR_LENGTH(nombre)>8',
        ]);
    }
    
    public function actionConsulta3() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT COUNT(*) AS "numero_dioses" FROM dios',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['numero_dioses'],
                    "titulo" => "Consulta 3",
                    "enunciado" => "Indica el numero total de dioses",
                    "sql" => 'SELECT COUNT(*) AS "numero_dioses" FROM dios',
        ]);
    }
    
    public function actionConsulta4() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT COUNT(DISTINCT(modo)) AS "numero_modos" FROM partida',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['numero_modos'],
                    "titulo" => "Consulta 4",
                    "enunciado" => "Indica el numero total de modos",
                    "sql" => 'SELECT COUNT(DISTINCT(modo)) AS "numero_modos" FROM partida',
        ]);
    }
    
    public function actionConsulta5() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT categoria, COUNT(*) FROM dios GROUP BY categoria',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['categoria','COUNT(*)'],
                    "titulo" => "Consulta 5",
                    "enunciado" => "Seleccionar el numero de dioses de cada categoria",
                    "sql" => 'SELECT categoria, COUNT(*) FROM dios GROUP BY categoria',
        ]);
    }
    
    public function actionConsulta6() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT modo,SUM(numuertes) FROM partida GROUP BY modo',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['modo','SUM(numuertes)'],
                    "titulo" => "Consulta 6",
                    "enunciado" => "Seleccionar el numero de muertes de cada modo",
                    "sql" => 'SELECT modo,SUM(numuertes) FROM partida GROUP BY modo',
        ]);
    }
    
    public function actionConsulta7() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT modo,SUM(numasistencias) FROM partida GROUP BY modo',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['modo','SUM(numasistencias)'],
                    "titulo" => "Consulta 7",
                    "enunciado" => "Seleccionar el numero de asistencias que se producen en cada modo de partida",
                    "sql" => 'SELECT modo,SUM(numasistencias) FROM partida GROUP BY modo',
        ]);
    }
    
    public function actionConsulta8() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nombre FROM dios d INNER JOIN participa p ON d.codigo = p.codigo WHERE numpartida=1',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nombre'],
                    "titulo" => "Consulta 8",
                    "enunciado" => "Seleccionar el nombre de los dioses de la partida 1",
                    "sql" => 'SELECT nombre FROM dios d INNER JOIN participa p ON d.codigo = p.codigo WHERE numpartida=1',
        ]);
    }
    
    public function actionConsulta9() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nombre FROM dios WHERE codigo NOT IN (SELECT codigo FROM participa)',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nombre'],
                    "titulo" => "Consulta 9",
                    "enunciado" => "Indicar que dios no ha participado en ninguna partida",
                    "sql" => 'SELECT nombre FROM dios WHERE codigo NOT IN (SELECT codigo FROM participa)',
        ]);
    }
    
    public function actionConsulta10() {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nombre FROM dios d INNER JOIN participa p ON d.codigo = p.codigo WHERE numpartida=(SELECT numpartida FROM partida WHERE numbajas=(SELECT MAX(numbajas) FROM partida))',
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nombre'],
                    "titulo" => "Consulta 10",
                    "enunciado" => "Indica el dios que haya hecho mas bajas en una partida",
                    "sql" => 'SELECT nombre FROM dios d INNER JOIN participa p ON d.codigo = p.codigo WHERE numpartida=(SELECT numpartida FROM partida WHERE numbajas=(SELECT MAX(numbajas) FROM partida))',
        ]);
    }
}
