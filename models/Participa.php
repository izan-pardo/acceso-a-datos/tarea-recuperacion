<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "participa".
 *
 * @property int $codigo
 * @property int $numpartida
 *
 * @property Dios $codigo0
 * @property Partida $numpartida0
 */
class Participa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'participa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'numpartida'], 'required'],
            [['codigo', 'numpartida'], 'integer'],
            [['codigo', 'numpartida'], 'unique', 'targetAttribute' => ['codigo', 'numpartida']],
            [['codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Dios::className(), 'targetAttribute' => ['codigo' => 'codigo']],
            [['numpartida'], 'exist', 'skipOnError' => true, 'targetClass' => Partida::className(), 'targetAttribute' => ['numpartida' => 'numpartida']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'numpartida' => 'Numpartida',
        ];
    }

    /**
     * Gets query for [[Codigo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigo0()
    {
        return $this->hasOne(Dios::className(), ['codigo' => 'codigo']);
    }

    /**
     * Gets query for [[Numpartida0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumpartida0()
    {
        return $this->hasOne(Partida::className(), ['numpartida' => 'numpartida']);
    }
}
