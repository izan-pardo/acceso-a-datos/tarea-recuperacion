<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dios".
 *
 * @property int $codigo
 * @property string $nombre
 * @property string $panteon
 * @property string $rol
 * @property string $categoria
 *
 * @property Participa[] $participas
 * @property Partida[] $numpartidas
 */
class Dios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'panteon', 'rol', 'categoria'], 'required'],
            [['nombre', 'panteon'], 'string', 'max' => 25],
            [['rol'], 'string', 'max' => 10],
            [['categoria'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'panteon' => 'Panteon',
            'rol' => 'Rol',
            'categoria' => 'Categoria',
        ];
    }

    /**
     * Gets query for [[Participas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipas()
    {
        return $this->hasMany(Participa::className(), ['codigo' => 'codigo']);
    }

    /**
     * Gets query for [[Numpartidas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumpartidas()
    {
        return $this->hasMany(Partida::className(), ['numpartida' => 'numpartida'])->viaTable('participa', ['codigo' => 'codigo']);
    }
}
