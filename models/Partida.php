<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partida".
 *
 * @property int $numpartida
 * @property string $modo
 * @property int $numbajas
 * @property string $numuertes
 * @property int $numasistencias
 *
 * @property Participa[] $participas
 * @property Dios[] $codigos
 */
class Partida extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partida';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numpartida', 'modo', 'numbajas', 'numuertes', 'numasistencias'], 'required'],
            [['numpartida', 'numbajas', 'numasistencias'], 'integer'],
            [['modo'], 'string', 'max' => 15],
            [['numuertes'], 'string', 'max' => 2],
            [['numpartida'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numpartida' => 'Numpartida',
            'modo' => 'Modo',
            'numbajas' => 'Numbajas',
            'numuertes' => 'Numuertes',
            'numasistencias' => 'Numasistencias',
        ];
    }

    /**
     * Gets query for [[Participas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipas()
    {
        return $this->hasMany(Participa::className(), ['numpartida' => 'numpartida']);
    }

    /**
     * Gets query for [[Codigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigos()
    {
        return $this->hasMany(Dios::className(), ['codigo' => 'codigo'])->viaTable('participa', ['numpartida' => 'numpartida']);
    }
}
